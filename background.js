browser.contextMenus.create({
    id: "download-playstv",
    title: "Download your videos",
    contexts: ["page"]
  });

  browser.contextMenus.onClicked.addListener(function(info, tab) {
    if (info.menuItemId == "download-playstv") {
      downloadPlaysTvClicked();
    }
  });

var downloadQueue = [];

function downloadPlaysTvClicked() {
    const remoteCode = "execute();";

    browser.tabs.executeScript({
        code: "typeof execute === 'function';",
    }).then((results) => {
        // The content script's last expression will be true if the function
        // has been defined. If this is not the case, then we need to run
        // clipboard-helper.js to define function copyToClipboard.
        if (!results || results[0] !== true) {
            return browser.tabs.executeScript(  {
                file: "content_script.js",
            });
        }
    }).then(() => {
        return browser.tabs.executeScript(  {
            code: remoteCode
        });
    }).then(() => {
        console.log("Starting queue");
        downloadVideo(downloadQueue.pop());
    
    }).catch((error) => {
        // This could happen if the extension is not allowed to run code in
        // the page, for example if the tab is a privileged page.
        console.error("Failed to copy text: " + error);
    });
}


browser.runtime.onMessage.addListener(enqueDownloadVideo);

function handleDownloadChanged(delta) {
    if (delta.state && delta.state.current === "complete") {
      console.log(`Download ${delta.filename} was saved.`);
      var d = downloadQueue.pop();
      if (d != null) {
        downloadVideo(d);
      }
    } 
}

function enqueDownloadVideo(message) {
    console.log(`received message ${message}`)
    if (message.type == "download") {
        var i = message.data;
        console.log(`adding ${i} to download queue`);
        //downloadQueue.push(i);
        downloadVideo(i);
    }
  }

function downloadVideo(i) {
    if (!i) {
        console.log("i was undefined, aborting");
        return;
    }
    console.log(`Downloading ${i}`);
    return browser.downloads.download(
    {
        url: i.url,
        filename: i.filename,
        conflictAction : 'uniquify',
        saveAs: false
    });
  }