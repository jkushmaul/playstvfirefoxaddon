//Load your videos:  https://plays.tv/u/CptCanada
//so you can see "your videos"

var lastScrollHeight = 0;
function execute() {
    if (lastScrollHeight == 0) {
        window.scrollTo(0, 0);
    }
    if (document.body.scrollHeight > lastScrollHeight) {
        console.log(`scrolling: ${lastScrollHeight} < ${document.body.scrollHeight}`)
        lastScrollHeight = document.body.scrollHeight;
        window.scrollTo(0, document.body.scrollHeight);
        
        setTimeout(execute, 2000);
    } else {
        console.log(`Finished: ${lastScrollHeight} < ${document.body.scrollHeight}`)
        downloadPlaysTvVideos();
    }
}

function downloadPlaysTvVideos() {
  
    var count = 0;
    console.log("Starting scrape");
    document.querySelector('div.mod-user-videos').querySelectorAll('li.video-item').forEach( function(li) {
        
  
        console.log(`looking at ${li}`);
        var title = li.querySelector('div.info > a').textContent;
        var filename = title.replace(/[,./\\?%*:|"<>]/g, '-') + ".mp4";
        var videoPreviewUrl  = li.querySelector('video.video-tag > source').getAttribute("src");
        
        
   
        var downloadUrl = videoPreviewUrl.match("^(.*)+/preview")[1] + "/720.mp4";
        console.log(`sending message for ${downloadUrl}`);
        browser.runtime.sendMessage({
             type: "download", data: {url: "https:" + downloadUrl, filename: "" + filename}
        });
        
        count ++;
    } );
 
}