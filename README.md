## PlaysTv downloader
 
![](https://gitlab.com/jkushmaul/playstvfirefoxaddon/raw/master/icon.png "Plays.tv downloader")

I liked plays.tv, but they really screwed their users by making
them manage their own downloads.  It's almost as if they went out of their way
to make this difficult.

1. It uses a shitty filename - the res of the video, so manually you can't just bulldoze
through and download them by clicking the links.
2. It uses javascript which makes it hard for download managers to download.

# How to use this

1. clone the source
2. Open firefox
3. Right click and save something where you want the files.
4. visit about:debugging#/setup
5. click "This Firefox"
6. "Load Temporary Addon..."
7. Browse to source,  select "manifest.json", click "Open".
8. Visit https://plays.tv/profile
9. Right click and select "Download plays.tv videos".
10. Observe the inifinite scroll
11. Visit about:downloads and see the downloads begin.
